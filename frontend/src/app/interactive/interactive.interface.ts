export interface CarInterface {
    id: number
    brand: string
    name: string
    value: number,
    description?: string
}