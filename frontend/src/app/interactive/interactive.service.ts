import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CarInterface } from './interactive.interface';

@Injectable({
    providedIn: 'root'
})
export class InteractiveService {

    baseUrl = "http://localhost:8000/listra-cars"

    constructor(private http: HttpClient) { }

    read(): Observable<CarInterface[]>{
        return this.http.get<CarInterface[]>(this.baseUrl)
    }

}