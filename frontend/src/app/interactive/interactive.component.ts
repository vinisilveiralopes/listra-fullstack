import { Component, OnInit, Input } from '@angular/core';
import { CarInterface } from './interactive.interface';
import * as $ from 'jquery';
import { InteractiveService } from './interactive.service';

@Component({
  selector: 'app-interactive',
  templateUrl: './interactive.component.html',
  styleUrls: ['./interactive.component.scss']
})

export class InteractiveComponent implements OnInit {

  @Input() car: CarInterface
  
  public cars: CarInterface[] = []

  constructor(private interactiveService?: InteractiveService) {}
  
  ngOnInit(): void {
    this.interactiveService.read().subscribe(car => {
      this.cars = car
    })
  }

  changeVehicle(id: number) {

    $('#signal').val('');

    $('.cards').slideUp();

    // Change car label name
    let carName: string = `${this.cars[id-1].brand} - ${this.cars[id-1].name}`;
    $('#carName').html(carName)

    // Format car value to set into html element
    let v = (this.cars[id-1].value).toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL',
    }); 

    $('.simulate').val(this.cars[id-1].value);

    // Change car value
    $('#carValue').html(`${v} `) 

    $('#description').html(`${this.cars[id-1].description}`) 

    $('.car-desc').slideDown();
    
    $('.car-img').html(`<img style='width: 90%' src="assets/img/${id}.png">`)
  }

}
