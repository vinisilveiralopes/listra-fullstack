import { Component, OnInit } from '@angular/core';
import { InteractiveComponent } from '../interactive/interactive.component';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  simulate(v: number){

    let totalV = v;

    let parcialV : any = $('#signal').val() == '' ? 0 : $('#signal').val();

    let x48 = ((totalV - parcialV) / 48).toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL',
    });

    let x12 = ((totalV - parcialV) / 12).toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL',
    })
    ;
    let x6 = ((totalV - parcialV) / 6).toLocaleString('pt-BR', {
      style: 'currency',
      currency: 'BRL',
    });

    $('#card48').html(`${x48}`);
    $('#card12').html(`${x12}`);
    $('#card6').html(`${x6}`);
    
    $('.cards').slideDown();
  }
}
