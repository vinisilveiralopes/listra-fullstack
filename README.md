# listra-fullstack
Author: Vinicius Silveira Lopes
Date: 07/09/2020 05:30 PM
 -> Used to evaluate skills.


-------------------
- - - **BACKEND** - - -
------------------- 

    OBS: CRIE UM ARQUIVO .env COM BASE NO ARQUIVO .env.example

    #1 Editar o arquivo .env e apontar um banco de dados MySql válido
    #2 Executar o comando: `composer i`
    #3 Executar o comando para criação de tabela: `php artisan migrate`
    #4 Executar o comando para popular os dados no banco: `php artisan db:seed`
    #5 Executar o comando: `php artisan serve` (para disponibilizar o projeto localmente)
    #6 Realizado as etapas anteriores. O backend estara disponivel no endereço: localhost:8000

--------------------
- - - **FRONTEND** - - -
--------------------

    #1 Acessar a pasta e executar o comando: `npm i` (instalar todas as dependências)
    #2 Executar o comando: `ng serve`
    #3 O frontend estará disponível no endereço: localhost:4200


