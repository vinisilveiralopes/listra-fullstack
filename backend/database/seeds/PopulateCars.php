<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PopulateCars extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        DB::table('cars')->insert([
            [
                'description' => 'O Quattroporte de 2019 oferece um espaço abundante e elegante. O estilo esculpido, inegavelmente agressivo mas também extremamente gracioso, tem o poder evocativo que se esperaria da Maserati.', 
                'name' => 'Quattroporte',  
                'brand' => 'Maserati',
                'value' => 134000,
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'description' => 'O SUV Peugeot 3008 chegou ao Brasil trazendo todo o sucesso que fez dele um dos carros mais premiados de sua categoria no Mundo, com mais de 38 prêmios.', 
                'name' => '3008 SUV',  
                'brand' => 'Peugeot',
                'value' => 128990,
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'description' => 'Sua taxa de compressão alta garante a eficiência energética, sem contar que ele tem uma autonomia de 600km na cidade e 700km na estrada.', 
                'name' => 'Argo',  
                'brand' => 'FIAT',
                'value' => 61500,
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'description' => 'Ele tem motor 2.0 turbo, com 230 cv e 35,7 kgfm de torque, com aceleração de 0 a 100 km/h em 7 segundos e velocidade máxima de 238 km/h.', 
                'name' => 'Golf GTI',  
                'brand' => 'Volkswagen',
                'value' => 114900,
                'created_at' => date('Y-m-d H:i:s'), 
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ]);
    }
}
